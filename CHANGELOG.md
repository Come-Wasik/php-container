# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 1.0.2 - 2020-10-31

Add an interface `ExtendedContainerInterface` for personnal methods added in the BaseContainer, as delete().
Add one method in BaseContainer (so in 2 types of containers too):
+ deleteAll()

Update methods in BaseContainer, no change occured. I factorised methods which needed it.

## 1.0.1

Modify IndexedContainer::push function to using the native php function array_push instead of []

## 1.0.0

Initial stable release.