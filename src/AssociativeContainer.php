<?php

namespace Nolikein\Container;

use InvalidArgumentException;
use Nolikein\Container\BaseContainer;
use Nolikein\Container\Interfaces\AssociativeContainerInterface;

/**
 * @method get($id)             Get a data by id
 * @method has($id)             Check if has data by id
 * @method all()                Get all data
 * @method delete($id)          Delete a data by id
 * @method set($name, $data)    Set a data by name
 */
class AssociativeContainer extends BaseContainer implements AssociativeContainerInterface
{
    public function set($name, $data): void
    {
        if (!is_numeric($name) && !is_string($name)) {
            throw new InvalidArgumentException('The argument "' . $name . '" need to be a numeric (int, float) or a string', 500);
        }

        $this->backpack[$name] = $data;
    }
}
