<?php

namespace Nolikein\Container;

use Nolikein\Container\Exception\ContainNullException;
use Nolikein\Container\Exception\NotContainException;
use Nolikein\Container\Interfaces\ExtendedContainerInterface;
use Psr\Container\ContainerInterface;

abstract class BaseContainer implements ContainerInterface, ExtendedContainerInterface
{
    /** @var mixed[] $backpack A container array */
    protected $backpack = [];


    public function get($id)
    {
        if (!$this->has($id)) {
            throw new NotContainException(__CLASS__, $id);
        }

        $retrievedData = $this->backpack[$id];

        if (is_null($retrievedData)) {
            throw new ContainNullException(__CLASS__, $id);
        }

        return $retrievedData;
    }

    public function has($id): bool
    {
        if (!key_exists($id, $this->backpack)) {
            return false;
        }
        return true;
    }

    public function all(): array
    {
        return $this->backpack;
    }

    public function delete($id): void
    {
        if (!key_exists($id, $this->backpack)) {
            throw new NotContainException(__CLASS__, $id);
        }

        unset($this->backpack[$id]);
    }

    public function deleteAll(): void
    {
        foreach($this->all() as $key => $element) {
            $this->delete($key);
        }
    }
}
