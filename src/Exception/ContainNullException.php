<?php

namespace Nolikein\Container\Exception;

use Exception;
use Psr\Container\ContainerExceptionInterface;

class ContainNullException extends Exception implements ContainerExceptionInterface
{
    // Redefine the exception so message isn't optional
    public function __construct(string $containerName, string $dataId)
    {
        parent::__construct('The container "' . $containerName . '" with data "' . $dataId . '" contain null', 500, null);
    }

    // custom string representation of object
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
