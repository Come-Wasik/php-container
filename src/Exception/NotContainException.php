<?php

namespace Nolikein\Container\Exception;

use Exception;
use Psr\Container\NotFoundExceptionInterface;

class NotContainException extends Exception implements NotFoundExceptionInterface
{
    // Redefine the exception so message isn't optional
    public function __construct(string $containerName, string $dataId)
    {
        parent::__construct('The container "' . $containerName . '" has not the wanted data "' . $dataId . '"', 500, null);
    }

    // custom string representation of object
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
