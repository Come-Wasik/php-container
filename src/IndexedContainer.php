<?php

namespace Nolikein\Container;

use Nolikein\Container\BaseContainer;
use Nolikein\Container\Interfaces\IndexedContainerInterface;

/**
 * @method get($id)         Get data by id
 * @method has($id)         Check if has data by id
 * @method all()            Get all data
 * @method delete($id)      Delete a data by id
 * @method push($data)      Set a data onto the end of the container
 * @method unshift($data)   Set a data onto the start of the container
 * @method pop()            Drop and get a data from the end of the container
 * @method shift()          Drop and get a data from the start of the container
 */
class IndexedContainer extends BaseContainer implements IndexedContainerInterface
{
    public function push($data): void
    {
        array_push($this->backpack, $data);
    }

    public function unshift($data): void
    {
        array_unshift($this->backpack, $data);
    }

    public function pop()
    {
        return array_pop($this->backpack);
    }

    public function shift()
    {
        return array_shift($this->backpack);
    }
}
