<?php

namespace Nolikein\Container\Interfaces;

interface AssociativeContainerInterface
{
    /**
     * Set $data to the container associated with a $name
     *
     * The name can be a string or an id
     *
     * @param string|int $name The name associated with the $data
     * @param mixed $data A data to add/modify in the container
     *
     * @throws InvalidArgumentException The $name is neither an int or a string
     */
    public function set($name, $data): void;
}
