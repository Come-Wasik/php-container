<?php

namespace Nolikein\Container\Interfaces;

/**
 * The extended version of the container interface allow to
 * upgrade the psr container interface.
 * 
 * This interface allow you to get all and delete elements
 * in the container.
 */
interface ExtendedContainerInterface
{
    /**
     * Retrieve all data stocked in the container.
     *
     * @return mixed[] Return all data stocked in the container.
     */
    public function all(): array;

    /**
     * Delete a data stocked in the container.
     *
     * @param mixed $id The unique identifier of the data you want to delete.
     * 
     * @throws NotContainException The $id element does not exist.
     */
    public function delete($id): void;

    /**
     * Delete all data stocked in the container.
     */
    public function deleteAll(): void;
}