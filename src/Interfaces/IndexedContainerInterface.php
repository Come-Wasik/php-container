<?php

namespace Nolikein\Container\Interfaces;

use Psr\Container\ContainerInterface;

interface IndexedContainerInterface extends ContainerInterface
{
    /**
     * Set $data at the end of the container
     *
     * @param mixed $data A data to add in the container
     */
    public function push($data): void;

    /**
     * Set $data at the start of the container
     *
     * @param mixed $data A data to add in the container
     */
    public function unshift($data): void;

    /**
     * Drop and get a data from the end of the container
     *
     * @return mixed The data dropped from the end of the container
     */
    public function pop();

    /**
     * Drop and get a data from the start of the container
     *
     * @return mixed The data dropped from the start of the container
     */
    public function shift();
}
