<?php

declare(strict_types=1);

namespace Test;

use PHPUnit\Framework\TestCase;
use Nolikein\Container\AssociativeContainer;
use Nolikein\Container\Container;
use Nolikein\Container\IndexedContainer;

class ContainerTest extends TestCase
{
    public function testAssociativeContainer()
    {
        $associativeContainer = new AssociativeContainer();

        // Set
        $associativeContainer->set('test1', 'data1');
        $associativeContainer->set('test2', 'data2');
        $associativeContainer->set(15, 12.8);
        $associativeContainer->set(12.5, []);

        // Has
        $this->assertTrue($associativeContainer->has('test1'));
        $this->assertFalse($associativeContainer->has('test4'));

        // Get
        $this->assertEquals('data1', $associativeContainer->get('test1'));

        // All
        $this->assertIsArray($associativeContainer->all());
        $this->assertEquals('data1', $associativeContainer->all()['test1']);

        // Delete
        $this->assertTrue($associativeContainer->has('test2'));
        $associativeContainer->delete('test2');
        $this->assertFalse($associativeContainer->has('test2'));

        $associativeContainer->deleteAll();
        $this->assertEmpty($associativeContainer->all());
    }

    public function testIndexedContainer()
    {
        $indexedContainer = new IndexedContainer();

        // Push
        $indexedContainer->push('data1');
        $indexedContainer->push(123);
        $indexedContainer->push([]);

        // Has
        $this->assertTrue($indexedContainer->has(0));
        $this->assertFalse($indexedContainer->has(18));

        // Get
        $this->assertEquals('data1', $indexedContainer->get(0));
        $this->assertEquals([], $indexedContainer->get(2));

        // All
        $this->assertIsArray($indexedContainer->all());
        $this->assertEquals('data1', $indexedContainer->all()[0]);

        // Delete
        $this->assertTrue($indexedContainer->has(1));
        $indexedContainer->delete(1);
        $this->assertFalse($indexedContainer->has(1));

        // Unshift and shift
        $indexedContainer->unshift('newData');
        $this->assertEquals('newData', $indexedContainer->get(0));
        $this->assertEquals('newData', $indexedContainer->shift());

        // Push and pop
        $this->assertEquals([], $indexedContainer->pop());
    }
}
